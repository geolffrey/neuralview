NeuralView
==========

About
-----

NeuralView is a graphical interface for FANN [1], making possible
to graphically design, train, and test artificial neural networks.

There are packages for Ubuntu 13.10 on *ppa:lucashnegri/lua*.

License
-------

NeuralView is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dependencies
------------

* GTK >= 3.8
* lgob >= 13.07 (modules: gobject, gtk, cairo)
* lcl >= 1.1
* lfann >= 1.3.0
* gnuplot (optional, for error plots)

Contact
-------

Lucas Hermann Negri <lucashnegri@gmail.com>

[1]: http://leenissen.dk/fann/
